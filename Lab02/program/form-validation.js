function getTips(){
	var price = $( "#price" ).val();
	var service = $( "#service" ).val();
	var food = $( "#food" ).val();
	var tip;
	
	var lowTip = 0.05;
	var averTip = 0.15;
	var highTip = 0.25;
	var tipRange = highTip-lowTip;
	var badService = 0;
	var okayService = 3; 
	var goodService = 7;
	var greatService = 10;
	var serviceRange = greatService-badService;
	var badFood = 0;
	var greatFood = 10;
	var foodRange = greatFood-badFood;

	if (service<okayService){
		tip = (((averTip-lowTip)/(okayService-badService))*service+lowTip)*serviceRange +(1-serviceRange)*(tipRange/foodRange*food+lowTip);
	}
	else if (service<goodService) {
		tip = averTip*serviceRange + (1-serviceRange)*(tipRange/foodRange*food+lowTip);
	}
	else {
		tip = (((highTip-averTip)/ (greatService-goodService))* (service-goodService)+averTip)*serviceRange + (1-serviceRange)*(tipRange/foodRange*food+lowTip);	
	}
	
	
	$( "#tips" ).text("Чайові ("+tip.toFixed(3)+"): "+(tip*price).toFixed(1));
}
