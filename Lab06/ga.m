FitnessFunction = @shufcn;
numberOfVariables = 2;
rng default % Для відтворюваності
[x,Fval,exitFlag,Output] = ga(FitnessFunction,numberOfVariables);
fprintf('The number of generations was : %d\n', Output.generations);
fprintf('The number of function evaluations was : %d\n', Output.funccount);
fprintf('The best function value found was : %g\n', Fval);
opts = gaoptimset ('PlotFcn',{@gaplotbestf,@gaplotstopping});
[x,Fval,exitFlag,Output] = ga(FitnessFunction,numberOfVariables,[],[],[],[],[],[],[],opts);




opts.PopulationSize = 10;
Population = rand(3,2)
opts.InitialPopulationRange = [-1 0; 1 2];
[x,Fval,exitFlag,Output] = ga(FitnessFunction,numberOfVariables,[],[],[], ...
    [],[],[],[],opts);
fprintf('The number of generations was : %d\n', Output.generations);
fprintf('The number of function evaluations was : %d\n', Output.funccount);
fprintf('The best function value found was : %g\n', Fval);



clear all
plotobjective(@shufcn,[-2 2; -2 2]);
FitnessFunction = @shufcn;
numberOfVariables = 2;
 
% thestate = rng;
% rng(thestate);
opts.PopulationSize = 10;% размер популяции
%Population = rand(3,2)
 
opts = gaoptimset('PlotFcn',{@gaplotbestf,@gaplotstopping});
[x,Fval,exitFlag,Output] = ...
    ga(FitnessFunction,numberOfVariables,[],[],[],[],[],[],[],opts);
 
[x,Fval,exitFlag,Output] = ga(FitnessFunction,numberOfVariables);
fprintf('The number of generations was : %d\n', Output.generations);
fprintf('The number of function evaluations was : %d\n', Output.funccount);
fprintf('The best function value found was : %g\n', Fval);




opts = optimset('MaxGenerations',150,'MaxStallGenerations', 100);
[x,Fval,exitFlag,Output] = ga(FitnessFunction,numberOfVariables,[],[],[],[],[],[],[],opts);
fprintf('The number of generations was : %d\n', Output.generations);
fprintf('The number of function evaluations was : %d\n', Output.funccount);
fprintf('The best function value found was : %g\n', Fval);




opts = gaoptimset('SelectionFcn',@selectiontournament, ...
                        'FitnessScalingFcn',@fitscalingprop);

[x,Fval,exitFlag,Output] = ga(FitnessFunction,numberOfVariables,[],[],[], ...
    [],[],[],[],opts);

fprintf('The number of generations was : %d\n', Output.generations);
fprintf('The number of function evaluations was : %d\n', Output.funccount);
fprintf('The best function value found was : %g\n', Fval);



function y = simple_fitness (x)
y = 100 * (x (1) ^ 2 - x (2)) ^ 2 + (1 - x (1)) ^ 2;
FitnessFunction = @simple_fitness;
numberOfVariables = 2;
[x,fval] = ga(FitnessFunction,numberOfVariables)



FitnessFunction = @(x) vectorized_fitness(x,100,1);
numberOfVariables = 2;
options = gaoptimset('UseVectorized',true);
[x,fval] = ga(FitnessFunction,numberOfVariables,[],[],[],[],[],[],[],options)


